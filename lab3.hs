-- Suranda max domino kieki NxM lentoje
getList :: Read read => IO [read]

getList = fmap (map read . words) getLine

solve :: Int -> Int -> Int
solve m n = m * n `div` 2

main :: IO ()
main = do
        putStrLn "Ivesti lentos dimensijas n,m"
        [m,n] <- getList
        putStrLn . show $ solve m n